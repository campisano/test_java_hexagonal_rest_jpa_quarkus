#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail



git symbolic-ref HEAD --short 2>/dev/null || git name-rev --tags --name-only HEAD
